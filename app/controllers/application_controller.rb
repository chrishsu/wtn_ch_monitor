$API_KEY = "cnkFVAryqErEB3y8rnxHtGiTfyvisfhV"

class ApplicationController < ActionController::Base
	
  protect_from_forgery
  def execute_cmd(client, sql_command)
      results = client.query(sql_command)
      return Array(results)
  end
  
  def login
    if params[:key] == "AdCliikRocks!!!"
      session["role"] = "admin"
      redirect_to :controller => :dashboard
    elsif params[:key] == $API_KEY
      session["role"] = "Jashami"
      redirect_to :logs
    else
      session["role"] = nil
      redirect_to :root
    end
  end
  
  def get_post_params
    @post_body = @post_body ? @post_body : request.raw_post
    @post_params = ActiveSupport::JSON.decode(@post_body)
    @post_params["service"]
  end


  def authorize_permission
    if @post_params
      authorize_post_permission
    elsif params
      authorize_get_permission
    else
      render :json => {"status_code" => $W_N_STATUS["W_N_API_KEY_ERROR"]}
    end
  end

  def authorize_post_permission
    case @post_params["api_key"]
    when "e2712f5f21ab94c4ae70eb569188fdb1"
      @post_params["role"] = "admin"
    when "MTBUn5DTtVf2TdqehsEQKNK2rrLYRuPX"
      @post_params["role"] = "EatMe"
    when "QrpAdbwpknqcfEJoWwJVlheh5qlJJEVg"
      @post_params["role"] = "WTN_DinnerAndBreakfast"
    else
      render :json => {"status_code" => $W_N_STATUS["W_N_API_KEY_ERROR"]}
    end
  end

  def authorize_get_permission 
    case params[:api_key]
    when "e2712f5f21ab94c4ae70eb569188fdb1"
      params[:role] = "admin"
    when "MTBUn5DTtVf2TdqehsEQKNK2rrLYRuPX"
      params[:role] = "EatMe"
    else
      render :json => {:status_code => $W_N_STATUS["W_N_API_KEY_ERROR"]}
    end
  end

  def check_admin_permission
    if session["role"] != "admin"
      redirect_to :controller => :numbers, :action => :index
    end
  end

  def check_user_permission
    if (session["role"] != "admin") && (session["role"] != "Jashami")
      redirect_to :root
    end
  end
  
  def verify_api_key
    if params[:api_key] == $API_KEY
    else
      render :json => ["invalid api key"]
    end
  end

  def get_address_objs(phone, country_code)
    PhoneNumberTable.set_country(country_code)
    PhoneNumberToAddress.set_country(country_code)
    AddressTable.set_country(country_code)

    phone_obj = PhoneNumberTable.where(:phone_number => 
                  phone_completion(phone, country_code))[0]

    phone_to_address_objs = phone_obj ?
      PhoneNumberToAddress.where(:phone_number_id => phone_obj.id).order("confidence DESC") :
      []

    address_objs = phone_to_address_objs.map &lambda {|p_a_obj| AddressTable.find(p_a_obj.address_id)}
    address_objs
  end

  def get_addresses(phone, country_code)
    addresses = get_address_objs(phone, country_code).map &lambda {|a_obj| a_obj.address}
    addresses
  end


  def add_action(input,action)
    result = []
    input.each do |i|
      each = {}
      each["latitude"] = i.latitude
      each["longitude"] = i.longitude
      each["search_input"] = i.search_input
      each["created_at"] = i.created_at
      each["action"] = action
      result.push(each)
    end
    result
  end

  def weekdaycounts(data)
    fre = {"Mon"=>0,"Tue"=>0,"Wed"=>0,"Thu"=>0,"Fri"=>0,"Sat"=>0,"Sun"=>0}
    data.each do |r|
      case r["created_at"].strftime("%a")
        when "Fri"
          fre["Fri"] = fre["Fri"] + 1
        when "Sat"
          fre["Sat"] = fre["Sat"] + 1
        when "Sun"
          fre["Sun"] = fre["Sun"] + 1
        when "Mon"
          fre["Mon"] = fre["Mon"] + 1
        when "Tue"
          fre["Tue"] = fre["Tue"] + 1
        when "Wed"
          fre["Wed"] = fre["Wed"] + 1
        when "Thu"
          fre["Thu"] = fre["Thu"] + 1
      end
    end
    fre
  end

  def timecounts(data)
    fre = {"0-7"=>0,"7-11"=>0,"11-13"=>0,"13-17"=>0,"17-19"=>0,"19-21"=>0,"21-24"=>0}
    data.each do |r|
      case r["created_at"].strftime("%k").to_i
        when 0..6
          fre["0-7"] = fre["0-7"] + 1
        when 7..10
          fre["7-11"] = fre["7-11"] + 1
        when 11..12
          fre["11-13"] = fre["11-13"] + 1
        when 13..16
          fre["13-17"] = fre["13-17"] + 1
        when 17..18
          fre["17-19"] = fre["17-19"] + 1
        when 19..20
          fre["19-21"] = fre["19-21"] + 1
        when 21..23
          fre["21-24"] = fre["21-24"] + 1
      end
    end
    fre
  end

  def mcc_to_contry(mcc_code)
    if(mcc_code.blank? == false)
      mcc_code = mcc_code.to_s
      mcc_code = mcc_code[0,3]
      country = $mcc_to_country["#{mcc_code}"]
    else
      country = "N/A"
    end
    country
  end

  def findloc(lat,lon)
    url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=#{lat},#{lon}&sensor=true&region=tw"
    loc = open(url)
    loc = loc.read
    loc = ActiveSupport::JSON.decode(loc)
    if(loc['results'][1] != nil)
      result = loc['results'][1]['formatted_address'] 
    else
      result = "No Location Information"
    end
    result
  end

  def count_rate(part,total)
    p = part.to_f
    t = total.to_f
    rate = t == 0 ? 0 : (p/t).round(2)
    rate
  end

  def phone_completion(phone, country_code)
    phone = phone.delete("-() .+,")
    phone_code = $COUNTRY_CODE_2_CALLING_CODE[country_code]
    if phone[0..(phone_code.length - 1)] != phone_code
      if phone[0..0] == "0" or phone[0..0] == 0
        phone = phone_code + phone[1..-1]
      else
        phone = phone_code + phone
      end
    end
    phone
  end

  def rate_change_icon(rate)
    if(rate>0)
      icon = "http://icons.iconarchive.com/icons/visualpharm/must-have/16/Stock-Index-Up-icon.png"
    else
      icon = "http://icons.iconarchive.com/icons/visualpharm/must-have/16/Stock-Index-Down-icon.png"
    end
    icon
  end

  def rate_change_color(rate)
    if(rate>0)
      color="green"
    else
      color="red"
    end
    color
  end

end
