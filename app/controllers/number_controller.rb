class NumbersController < ApplicationController
  layout :monitor

  before_filter :check_admin_permission, :except => [:index, :login]

  def index
  end
  
  def search
  end
  
  def search_results
    search_input = params[:search_input].tr(" ","+")
    geo_code = params[:geo_code].upcase
    mcc_code = $COUNTRY_CODE_2_MCC[geo_code]
    @search_input = params[:search_input]
    @geo_code = geo_code

    first_level_result, @second_level_results = search_data(search_input, mcc_code)
    if first_level_result
      @first_level_table = first_level_result["first_level_table"]
      render :search_results
    else
      render :search
    end
  end

  def edit_result
    @cat = (params[:cat].blank?)? nil : params[:cat]
    @source = (params[:source].blank?)? nil : params[:source]
    @feedback_id = (params[:feedback_id].blank?)? nil : params[:feedback_id]
    @result = get_second_level_data(params[:first_level_table], params[:second_level_id])
    @search_input = params[:search_input]
    @geo_code = params[:geo_code]
    @first_level_table = params[:first_level_table]
    @second_level_id = params[:second_level_id]
    @addresses = get_addresses(@result["phone"], params[:geo_code])
    @modified_title = (params[:modified_title].blank?)? nil : params[:modified_title]
    @modified_link = (params[:modified_link].blank?)? nil : params[:modified_link]
    @modified_phone = (params[:modified_phone].blank?)? nil : params[:modified_phone]
    @modified_address = (params[:modified_address].blank?)? nil : params[:modified_address]
    
    render :edit_result
  end

  def save
    
    result = update_second_level_data(params[:first_level_table], 
                                      params[:second_level_id],
                                      params[:title],
                                      params[:phone],
                                      params[:original_link])
    puts result
    result = update_api_version(params[:search_input], params[:geo_code])
    puts result
    if(params[:source]=="feedback")
      redirect_to :back
    else
      redirect_to :action => :search_results, 
                  :search_input => params[:search_input], 
                  :geo_code => params[:geo_code]    
    end
  end
  
  def delete_result
    result = delete_second_level_data(params[:first_level_table], 
                                      params[:second_level_id])
    redirect_to :action => :search_results, 
                :search_input => params[:search_input], 
                :geo_code => params[:geo_code]                             
  end

  def delete_all_results
    mcc_code = $COUNTRY_CODE_2_MCC[params[:geo_code]]
    result = delete_first_level_data(params[:search_input], mcc_code)
    redirect_to :action => :search
  end

  def edit_address
    @cat = (params[:cat].blank?)? nil : params[:cat]
    @source = (params[:source].blank?)? nil : params[:source]
    @feedback_id = (params[:feedback_id].blank?)? nil : params[:feedback_id]
    @result = get_second_level_data(params[:first_level_table], params[:second_level_id])
    @search_input = params[:search_input]
    @geo_code = params[:geo_code]
    @first_level_table = params[:first_level_table]
    @second_level_id = params[:second_level_id]
    @modified_title = (params[:modified_title].blank?)? nil : params[:modified_title]
    @modified_link = (params[:modified_link].blank?)? nil : params[:modified_link]
    @modified_phone = (params[:modified_phone].blank?)? nil : params[:modified_phone]
    @modified_address = (params[:modified_address].blank?)? nil : params[:modified_address]
    @address_objs = get_address_objs(@result["phone"], params[:geo_code])
    @address_ids = @address_objs.map &lambda {|a_obj| a_obj.id}
  end
  
  def save_address
    cat = (params[:cat].blank?)? nil : params[:cat]
    source = (params[:source].blank?)? nil : params[:source]
    feedback_id = (params[:feedback_id].blank?)? nil : params[:feedback_id]
    if(params[:address_id]=="modified")
      result = add_address_data(params[:first_level_table],params[:second_level_id], params[:correct_address_modified],params[:geo_code])
      redirect_to :action => :edit_result,
                  :first_level_table => params[:first_level_table],
                  :second_level_id => params[:second_level_id],
                  :geo_code => params[:geo_code],
                  :feedback_id => feedback_id,
                  :cat => cat,
                  :search_input => params[:search_input],
                  :modified_link => params[:modified_link],
                  :modified_title => params[:modified_title],
                  :modified_phone => params[:modified_phone],
                  :modified_address => params[:modified_address],
                  :source => source
    elsif params[:address_id]
      update_address_set(params[:address_ids], 
                         params[:address_id], 
                         params["correct_address_#{params[:address_id]}"],
                         params[:geo_code])
            redirect_to :action => :edit_result,
                  :first_level_table => params[:first_level_table],
                  :second_level_id => params[:second_level_id],
                  :geo_code => params[:geo_code],
                  :feedback_id => feedback_id,
                  :cat => cat,
                  :search_input => params[:search_input],
                  :modified_link => params[:modified_link],
                  :modified_title => params[:modified_title],
                  :modified_phone => params[:modified_phone],
                  :modified_address => params[:modified_address],
                  :source => source
    else
      redirect_to :action => :edit_result,
                  :first_level_table => params[:first_level_table],
                  :second_level_id => params[:second_level_id],
                  :geo_code => params[:geo_code],
                  :feedback_id => feedback_id,
                  :cat => cat,
                  :search_input => params[:search_input],
                  :source => source

    end

  end
  
  def accept_each
    type = params[:type]
    table = params[:first_level_table]
    data = params[:modified_data]
    id = params[:second_level_id]
    sql_command = "Update #{table} set #{type} = '#{data}' where id=#{id}"
    execute_cmd($DB_CLIENT, sql_command)
    redirect_to :back
  end 
  
  def accept_change
    feedback_id = params[:feedback_id]
    feedback_data = ErrorFeedback.find_by_id(feedback_id)
    mcc_code = $COUNTRY_CODE_2_MCC[feedback_data["country_code"]]
    if(feedback_data["location"].blank?)
      search_input = feedback_data["search_input"]
    else
      search_input = feedback_data["search_input"]+"+"+feedback_data["location"]
    end
    search_input = search_input.tr(" ","+")
    f_data = search_data(search_input, mcc_code)

    table_name = f_data[0]["first_level_table"]
    link = feedback_data["link"]
    phone = feedback_data["phone"]
    s_data = get_second_level_data_by_phone_and_link(table_name, phone,link)

    second_level_id = s_data["id"]
    new_title = (feedback_data["modified_title"]==nil)? s_data["title"] : feedback_data["modified_title"]
    new_link = (feedback_data["modified_link"]==nil)? s_data["original_link"] : feedback_data["modified_link"]
    new_phone = (feedback_data["modified_phone"]==nil)? s_data["phone"] : feedback_data["modified_phone"]
    update_second_level_data(table_name, second_level_id, new_title, new_phone, new_link)

    encoded_phone = phone_completion(new_phone, feedback_data["country_code"])
    PhoneNumberTable.set_country(feedback_data["country_code"])
    phone_id=PhoneNumberTable.get_phone_id(encoded_phone)
    if(feedback_data["modified_address"]!=nil)
      AddressTable.set_country(feedback_data["country_code"])
      address_id = AddressTable.get_address_id(feedback_data["modified_address"])
      PhoneNumberToAddress.set_country(feedback_data["country_code"])
      max_confidence = PhoneNumberToAddress.get_max_confidence(phone_id)
      new_confidence = max_confidence + 1000
      exist_pair = PhoneNumberToAddress.where(:phone_number_id => phone_id, :address_id => address_id)
      if(exist_pair[0])
        exist_pair[0].confidence = new_confidence
        exist_pair[0].save
      else
        PhoneNumberToAddress.create(:phone_number_id => phone_id, :address_id => address_id, :confidence => new_confidence)
      end
    end
    feedback_data.handled = 1
    feedback_data.save
    redirect_to :controller => :error_feedback, :action => :index, :cat => "both"
  end

  def transfer_to_edit()
    search_input = params[:search_input]
    search_input = search_input.tr(" ","+")
    geo_code = params[:geo_code]
    link = params[:link]
    phone = params[:phone]
    mcc_code = $COUNTRY_CODE_2_MCC[geo_code]
    data = search_data(search_input, mcc_code)
    table_name = data[0]["first_level_table"]
    s_data = get_second_level_data_by_phone_and_link(table_name, phone, link)
    second_level_id = s_data["id"]
    #render :text =>second_level_id
    if(second_level_id.blank?)
      render :text => "Sorry, this data was changed, please modify it through search!"
    else
    redirect_to :controller => :numbers,
          :action => :edit_result,
                  :first_level_table => table_name,
                  :second_level_id => second_level_id,
                  :geo_code => geo_code,
                  :search_input => search_input,
                  :modified_title => params[:modified_title],
                  :modified_phone => params[:modified_phone],
                  :modified_link => params[:modified_link],
                  :modified_address => params[:modified_address],
                  :source => "feedback",
                  :cat => params[:cat],
                  :feedback_id => params[:feedback_id]
    end
  end

private
  def monitor
    "monitor"
  end

  def search_data(search_input, mcc_code)
    sql_command = "SELECT id, first_level_table FROM search_table 
                   WHERE search_input = \"#{search_input}\" and 
                   MCC_code = \"#{mcc_code}\""
    first_level_result = execute_cmd($DB_CLIENT, sql_command)[0]

    if first_level_result
      sql_command = "SELECT * FROM #{first_level_result['first_level_table']} 
                     WHERE search_input_id = #{first_level_result['id'].to_s}"
      second_level_results = execute_cmd($DB_CLIENT, sql_command)
    end
    [first_level_result, second_level_results]
  end
  
  def get_second_level_data_by_phone_and_link(table_name, phone, link)
      h = link.match("http")
      link_modified = ""
      if(h[0])
        link_modified = link.slice(7..link.length)
      else
        link_modified = "http://"+link
      end
      sql_command = "SELECT * FROM #{table_name} WHERE original_link= '#{link}' or original_link='#{link_modified}'"
      
      result = execute_cmd($DB_CLIENT, sql_command)
      
      data =''
      result.each do |r|
        if(r["phone"].include?(phone))
          data = r
          break
        end
      end
     
      data
      
  end
  
  def get_second_level_data(table_name, id)
    sql_command = "SELECT * FROM #{table_name} WHERE id = #{id}"
    result = execute_cmd($DB_CLIENT, sql_command)[0]
    result
  end
  
  def update_second_level_data(table_name, id, title, phone, original_link)
    sql_command = "UPDATE #{table_name} 
                   SET title = \"#{title}\",
                       phone = \"#{phone}\",
                       original_link = \"#{original_link}\"
                   WHERE id = #{id}"
    logger.info("sql_command: #{sql_command}")
    result = execute_cmd($DB_CLIENT, sql_command)[0]
    result
  end
  
  def delete_second_level_data(table_name, id)
    sql_command = "DELETE FROM #{table_name} 
                   WHERE id = #{id}"
    puts "sql_command = #{sql_command}"
    result = execute_cmd($DB_CLIENT, sql_command)[0]
    result
  end
  
  def get_address(phone, country_code)
    sql_command = "SELECT * FROM phone_number_table_#{country_code}
                   WHERE phone = #{phone} "
  end
  
  def delete_first_level_data(search_input, mcc_code)
    sql_command = "DELETE FROM search_table 
                   WHERE search_input = \"#{search_input}\" and 
                   MCC_code = \"#{mcc_code}\""
    logger.info("sql_command = #{sql_command}")
    result = execute_cmd($DB_CLIENT, sql_command)[0]
    result
  end

  def update_api_version(search_input, geo_code)
    mcc_code = $COUNTRY_CODE_2_MCC[geo_code]
    sql_command = "UPDATE search_table 
                   SET api_version = '0100'
                   WHERE search_input = \"#{search_input}\" and 
                   MCC_code = \"#{mcc_code}\""
    execute_cmd($DB_CLIENT, sql_command)[0]
  end
  def add_address_data(table_name,second_level_id,address,country_code)
    PhoneNumberTable.set_country(country_code)
    PhoneNumberToAddress.set_country(country_code)
    AddressTable.set_country(country_code)
    sql_command = "SELECT phone FROM #{table_name} WHERE id = #{second_level_id};"
    phone = execute_cmd($DB_CLIENT, sql_command)[0]["phone"]
    phone_completed = phone_completion(phone,country_code)
    phone_id = PhoneNumberTable.find_by_phone_number(phone_completed)
    if(phone_id.nil?)
      PhoneNumberTable.create(:phone_number => phone_completed,:frequency => 0, :valid_count => 0, :invalid_count => 0)
      phone_id = PhoneNumberTable.find_by_phone_number(phone_completed).id
    else
      phone_id = PhoneNumberTable.find_by_phone_number(phone_completed).id
    end

    address_id = AddressTable.get_address_id(address)
    max_confidence = PhoneNumberToAddress.get_max_confidence(phone_id)
    new_confidence = max_confidence + 1000
    
    exist_pair = PhoneNumberToAddress.where(:phone_number_id => phone_id, :address_id => address_id)
    if exist_pair[0]
      exist_pair[0].confidence = new_confidence
      exist_pair[0].save
    else
       PhoneNumberToAddress.create(:phone_number_id => phone_id, :address_id => address_id, :confidence => new_confidence)
    end
    
    
  end 

  def update_address_set(address_ids, target_id, target_address, country_code)
    PhoneNumberToAddress.set_country(country_code)
    AddressTable.set_country(country_code)

    # update address
    address_obj = AddressTable.find(target_id)
    address_obj.address = target_address
    address_obj.save

    # update confidence
    max_confidence = PhoneNumberToAddress.maximum("confidence")
    phone_to_address_obj = PhoneNumberToAddress.find_by_address_id(target_id)
    phone_to_address_obj.confidence = max_confidence + 1000
    phone_to_address_obj.save
  end
end
