class PhoneNumberTable < ActiveRecord::Base
  def self.set_country(country_code)
    self.table_name = "phone_number_table_#{country_code.upcase}"
    self.reset_column_information
  end

  def self.get_phone_id(phone)
    phone = phone.split(":")
    phone = phone[0]
  	log=self.select('id').where('phone_number=?',phone)
  	if(log[0]==nil)
  		self.create(:phone_number => phone, :valid_count => 0, :invalid_count => 0, :frequency => 0)
  		data = self.last
  		id = data['id']
  	else
  		id = log[0]['id']
  	end
  	id
  end	
end