class AddressTable < ActiveRecord::Base
  def self.set_country(country_code)
    self.table_name = "address_table_#{country_code.upcase}"
    self.reset_column_information
  end

  def self.get_address_id(address)
  	exisiting_address = self.find_by_address(address)
  	if(exisiting_address==nil)
	  	data = self.new(:address => address,:invalid_count => 0, :valid_count => 0,:confidence => 0)
	    data.save
	    last = self.last
	    id = last['id']
	else
		id = exisiting_address["id"]
	end
  end
end
