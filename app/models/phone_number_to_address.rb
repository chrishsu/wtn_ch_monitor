class PhoneNumberToAddress < ActiveRecord::Base
  def self.set_country(country_code)
    self.table_name = "phone_number_to_address_#{country_code.upcase}"
    self.reset_column_information
  end

  def self.get_max_confidence(phone_number_id)
  	log = self.select("max(confidence) as c").where("phone_number_id = ? ",phone_number_id)
	if(log[0]['c']==nil)
		max = 0
	else
		max = log[0]['c']
	end
	max

  end
end